#!/usr/bin/env bash
# script to package our goodies up and to plop them in ./deployment
# set -x

if [ ! -e "package.json" ]
then
    echo "Error: should be run from project root"
    exit 1
fi

ROOT_DIR="."
BUILD_DIR="${ROOT_DIR}/deployment"


if [ -a ${BUILD_DIR}/build.zip ]
then
    rm ${BUILD_DIR}/build.zip
fi

zip -r ${BUILD_DIR}/build.zip \
    package.json \
    res \
    src

