/**
 * User: curtis
 * Date: 12/12/17
 * Time: 9:08 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * A combination of commands that help us track stuff about our existence
 */

const _=require("lodash");
const fs=require("fs-extra");
const path=require("path");
const util=require("pig-core").util;
const {Command}=require("pig-cmd").command;
const file=require("pig-cmd").file;
const constant=require("../common/constant");
const model_factory=require("../data/factory");


/**
 * Adds metadata to our library - metadata-library.json
 */
class AddMetadataCommand extends Command {
	/**
	 * Looks for the last GetExifMetadata command in history and writes it to our library.
	 * Defaults error policy to debug
	 * @param {Object} options
	 * @param {string} options.library
	 */
	constructor(options=undefined) {
		super(Object.assign({
			errorPolicy: constant.severity.DEBUG
		}, options));
		this.sharePath=model_factory.store.get().share;
	}

	commands() {
		return [
			new file.EnsurePathCommand(this.sharePath),
			this
		];
	}

	execute(callback, history) {
		const self=this,
			metadata=this._getInputData(history),
			libraryPath=path.join(this.sharePath, "metadata-library.json"),
			library=util.try(fs.readJSONSync.bind(fs, libraryPath), {}),
			metaId=_.get(metadata, "FileName", "<none>"),
			directory=util.ensure(library, "directory", []).value;
		if(_.includes(directory, metaId)) {
			process.nextTick(callback);
		} else {
			const metaFileType=_.get(metadata, "FileType", "<unknown>").toLowerCase(),
				commonInfo=util.ensure(library, "common", {}).value,
				typeInfo=util.ensure(library, `types.${metaFileType}`, {}).value,
				merge=function(target) {
					// do the math on his properties
					_.each(metadata, function(value, key) {
						const valueType=self._valueToType(value),
							propertyInfo=util.ensure(target, key, {
								count: 0,
								type: [],
								source: []
							}).value;
						propertyInfo.count++;
						if(!_.includes(propertyInfo.type, valueType)) {
							propertyInfo.type.push(valueType);
						}
						propertyInfo.source.push({
							source: metaId,
							type: valueType,
							value: value
						});
					});
				};
			merge(commonInfo);
			merge(typeInfo);
			directory.push(metaId);
			directory.sort();
			fs.writeJSON(libraryPath, library, {
				spaces: "\t"
			}, callback);
		}
	}

	/**** Private Interface ****/
	/**
	 * Converts a exiftool type to an appropriate elasticsearch index type
	 * @param {String} value
	 * @returns {String}
	 */
	_valueToType(value) {
		const type=typeof(value);
		if(type==="number") {
			return (value.toString()===value.toFixed())
				? "integer"
				: "float";
		} else if(type==="string") {
			if(value.match(/^\d{4}:\d{2}:\d{2} \d{2}:\d{2}:\d{2}/)) {
				return "date";
			}
		}
		return type;
	}
}

exports.AddMetadata=AddMetadataCommand;
