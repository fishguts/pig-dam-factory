/**
 * User: curtis
 * Date: 11/15/17
 * Time: 11:05 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const fs=require("fs-extra");
const child_process=require("child_process");
const {PigError}=require("pig-core").error;
const {Command}=require("pig-cmd").command;
const http=require("../../common/http");

/**
 * Gets metadata about the file specified as a param
 */
class GetExifMetadataCommand extends Command {
	/**
	 * @param {String} contentPath assumed to be accessible by local exiftool
	 * @param {Object} options
	 */
	constructor(contentPath, options) {
		super(Object.assign({
			omitProperties: [
				"APP14Flags0",
				"APP14Flags1",
				"Directory",
				"Error",
				"ExifByteOrder",
				"ExifToolVersion",
				"FilePermissions",
				"FileInodeChangeDate",
				"SourceFile"
			]
		}, options));
		this.path=contentPath;
	}

	execute(callback) {
		const self=this,
			process=`exiftool -j ${this.path}`;
		child_process.exec(process, function(error, stdout) {
			// exif returns an error if he wasn't able to pull out exif data, but we are okay with that because
			// he gathers up some core info and until we have some other meta extraction tool or opt to use fstats
			// we will go with whatever he's given us provided there is something to stdout and we can parse it.
			if(!stdout) {
				callback(error || new PigError({
					cmd: process,
					message: "exiftool returned without results",
					statusCode: http.status.code.INTERNAL_SERVER_ERROR
				}));
			} else {
				try {
					// exiftool returns an array. Probably one element per input.
					let result=JSON.parse(stdout)[0],
						stats=fs.lstatSync(self.path);
					result=_.omit(result, self.getOption("omitProperties"));
					result.FileSize=stats.size;
					callback(null, result);
				} catch(error) {
					callback(error);
				}
			}
		});
	}
}

exports.GetExifMetadata=GetExifMetadataCommand;
