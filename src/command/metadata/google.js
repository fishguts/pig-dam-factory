/**
 * User: curtis
 * Date: 1/24/18
 * Time: 9:12 PM
 * Copyright @2018 by Xraymen Inc.
 */

const async=require("async");
const fs=require("fs-extra");
const path=require("path");
const {Command}=require("pig-cmd").command;
const {ReadFileCommand}=require("pig-cmd").file;
const {HttpPostRequestCommand}=require("pig-cmd").http.request;

// todo: will ultimately use a more secure form of auth
const GOOGLE_API_KEY="AIzaSyDwn-OPFoeZO_boFq7oZsQf6WkOB59bMn0";

/**
 * This guy acts as a router and passes the content on to an API that can make sense of it.
 */
class GetGoogleMetadataCommand extends Command {
	/**
	 * @param {String} contentPath assumed to be accessible locally
	 * @param {Object} options
	 */
	constructor(contentPath, options) {
		super(options);
		this.path=contentPath;
		this.workerCmd=null;
	}

	execute(callback, history) {
		if(this.workerCmd) {
			this.result=this.workerCmd.result;
		}
		process.nextTick(callback);
	}


	commands() {
		const parsed=path.parse(this.path);
		switch(parsed.ext.toLowerCase()) {
			case ".gif":
			case ".jpg":
			case ".png":
			case ".bmp": {
				return [
					this.workerCmd=new GoogleVisionCommand(this.path),
					this
				];
			}
			default: {
				// note: we want to make sure we get executed otherwise we don't make it into history
				return this;
			}
		}
	}
}

/**
 * Class good for getting meta info and suggestions for image content
 */
class GoogleVisionCommand extends Command {
	/**
	 * @param {String} contentPath assumed to be accessible locally
	 * @param {Object} options
	 *  - features {Array}
	 */
	constructor(contentPath, options) {
		super(options);
		this.path=contentPath;
		this.features=this.getOption("features", [
			"CROP_HINTS",
			"LABEL_DETECTION",
			"TEXT_DETECTION"
		]);
	}

	execute(callback, history) {
		const self=this;
		async.waterfall([
			function(done) {
				const requestBody={
					requests: [
						{
							features: self.features.map((feature)=>new Object({type: feature})),
							image: {
								content: history.lastOfType(ReadFileCommand).result
							}
						}
					]
				};
				const requestCmd=new HttpPostRequestCommand(`https://vision.googleapis.com/v1/images:annotate?key=${GOOGLE_API_KEY}`, {
					json: requestBody
				});
				requestCmd.execute(done);
			},
			function(result, done) {
				self.result=self._conditionGoogleResult(result);
				process.nextTick(done);
			}
		], callback);
	}

	commands() {
		return [
			new ReadFileCommand(this.path, {encoding: "base64"}),
			this
		];
	}

	/**** Private Interface ****/
	/**
	 * Picks apart the results and formats as per our spec.
	 * @param {Object} result
	 * @returns {{cropHints: *, labels: *, text: *}}
	 * @private
	 */
	_conditionGoogleResult(result) {
		return {
			cropHints: result.responses[0].cropHintsAnnotation.cropHints,
			labels: result.responses[0].labelAnnotations,
			text: result.responses[0].textAnnotations
		};
	}
}

exports.GetGoogleMetadata=GetGoogleMetadataCommand;
