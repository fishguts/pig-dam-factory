/**
 * User: curtis
 * Date: 1/24/18
 * Time: 9:22 PM
 * Copyright @2018 by Xraymen Inc.
 */

const exif=require("./exif");
const google=require("./google");

exports.GetExifMetadata=exif.GetExifMetadata;
exports.GetAIMetadata=google.GetGoogleMetadata;
