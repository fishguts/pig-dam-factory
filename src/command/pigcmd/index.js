/**
 * User: curtis
 * Date: 11/19/17
 * Time: 2:19 PM
 * Copyright @2017 by Xraymen Inc.
 *
 * This is the command interface to <link>./src/pigcmd.js</link>
 */

const metadata=require("../metadata/index");
const library=require("../library");
const queue=require("pig-cmd").queue;

const {Command}=require("pig-cmd").command;

/**
 * All actions supported by an implementing application
 * @type {Object<string, PigCliAction>}
 */
exports.ACTIONS={
	"addmeta": {
		args: "<respath>",
		desc: "adds a resource's type/metadata to our library",
		/**
		 * @param {Command} command
		 * @returns {function(callback)}
		 */
		handler: (command)=>command._addMetadataToLibrary.bind(command),
		/**
		 * @param {Array<string>} position
		 * @param {Object<string, string>} options
		 * @throws {Error} - if you want to fail validation
		 */
		validate: function(position, options) {
			if(position.length!==1) {
				throw new Error("must specify a resource path");
			}
		}
	}
};

/**
 * These are a complete list of all of the options we support on a command line:
 * @type {Object<string, PigCliAction>}
 */
exports.OPTIONS=[

];

/**
 * Interface into application specific commands
 */
class CommandInterface extends Command {
	constructor(action, options, position) {
		super(options);
		this.action=action;
		this.position=position;
		this.execute=exports.ACTIONS[this.action].handler(this);
	}

	/**** action handlers ****/

	/**
	 * Adds metadata to our library
	 * @param {Function} callback
	 * @private
	 */
	_addMetadataToLibrary(callback) {
		const series=new queue.SeriesQueue(this.options);
		series.addCommand(new metadata.GetExifMetadata(this.position[0]));
		series.addCommand(new library.AddMetadata({
			inputCommand: metadata.GetExifMetadata
		}));
		series.execute(callback);
	}
}

exports.CommandInterface=CommandInterface;
