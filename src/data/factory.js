/**
 * User: curtis
 * Date: 11/10/17
 * Time: 10:49 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const fs=require("fs-extra");
const path=require("path");
const db_model=require("./model");
const constant=require("../common/constant");
const notification=require("pig-core").notification;


const storage={
	/**
	 * Manages our instances per application: application.local, application.dev, etc.
	 */
	application: {},
	nodenv: (process.env.NODE_ENV || constant.nodenv.LOCAL)
};


/**
 * Gets/Sets application singleton
 */
exports.application={
	/**
	 * Gets a configuration for the specified application
	 * @param {Object} overrides
	 *  - nodenv {String}
	 *  - mode {String}
	 * @returns {Application}
	 */
	get: function(overrides=undefined) {
		let nodenv=storage.nodenv;
		if(_.has(overrides, "nodenv")) {
			nodenv=overrides.nodenv;
			overrides=_.omit(overrides, "nodenv");
		}
		if(storage.application[nodenv]) {
			// the application has already been configured nonetheless we will take overrides
			if(overrides) {
				_.merge(storage.application[nodenv].raw.application, overrides);
			}
		} else {
			exports.application.set(fs.readJSONSync(`./res/configuration/${nodenv}/settings.json`), overrides);
		}
		return storage.application[nodenv];
	},

	/**
	 * Sets a configuration for the specified application. The only time this should need to be called outside of the factory
	 * is when we are retrieving settings from our settings server
	 * @param {Object} settings
	 * @param {Object} overrides
	 * @returns {Application}
	 */
	set: function(settings, overrides=undefined) {
		const nodenv=settings.env.name;
		settings=_.cloneDeep(settings);
		// help keep the differences between apps easier to manage by setting up an "application" domain.
		// Once this is set then the differences in Application per module will be minimized
		settings.application=settings.module.factory;
		_.merge(settings.application, overrides);
		settings.application.store=new db_model.Store(Object.assign(
			settings.application.store,
			{
				dam: path.resolve(settings.application.store.dam),
				share: path.resolve(settings.application.store.share)
			})
		);
		storage.application[nodenv]=new db_model.Application(settings);
		this._applicationToEnvironment(storage.application[nodenv]);
		if(nodenv===storage.nodenv) {
			notification.emit(constant.event.SETTINGS_LOADED);
		}
		return storage.application[nodenv];
	},

	/**
	 * Aligns the environment to our application
	 * @param {Application} application
	 * @private
	 */
	_applicationToEnvironment: function(application) {
		process.env.DEBUG=application.debug.enabled;
		process.env.VERBOSE=application.debug.verbose;
	}
};

exports.store={
	/**
	 * Gets store singleton. It is also in the application
	 * @returns {Store}
	 */
	get: ()=>exports.application.get().store
};

exports.resourcePath={
	/**
	 * Constructs a project resource
	 * @param {String} relPath
	 * @returns {ResourcePath}
	 */
	create: function(relPath) {
		// todo: figure this out
		return new db_model.ResourcePath({
			absPath: path.join(exports.store.get().dam, relPath),
			relPath: relPath
		});
	}
};

