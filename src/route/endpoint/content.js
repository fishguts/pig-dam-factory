/**
 * User: curtis
 * Date: 11/20/17
 * Time: 9:46 PM
 * Copyright @2017 by Xraymen Inc.
 */

const file=require("pig-cmd").file;
const response=require("pig-cmd").http.response;
const {Route}=require("pig-cmd").route;
const metadata=require("../../command/metadata");
const library=require("../../command/library");
const model_factory=require("../../data/factory");

/**
 * Base class for all guys that have paths in their route
 */
class ResourceRoute extends Route {
	/**
	 * @param {ClientRequest} req
	 * @param {ServerResponse} res
	 * @param {Function} next
	 */
	constructor(req, res, next=undefined) {
		super(req, res, next);
		this._resource=model_factory.resourcePath.create(req.params.rootPath);
	}

	/**
	 * @returns {ResourcePath}
	 */
	get resource() { return this._resource; }
}

/**
 * Gets metadata for the specified path
 */
class GetExifMetadataRoute extends ResourceRoute {
	get schema() {
		return "./res/schema/route/metadata-get.json#request";
	}

	execute(callback) {
		const queue=this.createCommandQueue();
		queue.addCommand(new file.PathExistsCommand(this.resource.absPath));
		queue.addCommand(new metadata.GetExifMetadata(this.resource.absPath));
		queue.addCommand(new library.AddMetadata({
			inputCommand: metadata.GetExifMetadata
		}));
		queue.addCommand(new response.HttpSendJsonCommand(this.res, {
			inputCommand: metadata.GetExifMetadata
		}));
		queue.execute(callback);
	}
}

/**
 * Gets metadata for the specified path
 */
class GetAIMetadataRoute extends ResourceRoute {
	get schema() {
		return "./res/schema/route/metadata-get.json#request";
	}

	execute(callback) {
		const queue=this.createCommandQueue();
		queue.addCommand(new file.PathExistsCommand(this.resource.absPath));
		queue.addCommand(new metadata.GetAIMetadata(this.resource.absPath));
		queue.addCommand(new response.HttpSendJsonCommand(this.res, {
			inputCommand: metadata.GetAIMetadata
		}));
		queue.execute(callback);
	}
}

exports.GetExifMetadata=GetExifMetadataRoute;
exports.GetAIMetadata=GetAIMetadataRoute;
