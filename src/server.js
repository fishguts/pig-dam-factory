/**
 * User: curtis
 * Date: 11/10/17
 * Time: 9:24 PM
 * Copyright @2017 by Xraymen Inc.
 */

const {Command}=require("pig-cmd").command;
const {ExpressServerCommand}=require("pig-cmd").express;
const {SeriesQueue}=require("pig-cmd").queue;
const {SetupEnvironmentCommand}=require("./command/setup");
const data_factory=require("./data/factory");
const route_factory=require("pig-cmd").route.factory;
const route_content=require("./route/endpoint/content");
const route_diagnostics=require("./route/endpoint/diagnostics");

/**
 * We want to make sure the application environment is loaded before we do anything with it which is why we isolate the
 * commands in the startup command - they are all run after the environment has been loaded
 */
class StartupCommand extends Command {
	execute(callback) {
		const application=data_factory.application.get(),
			queue=new SeriesQueue({verbose: true}),
			server=new ExpressServerCommand({
				name: application.name,
				nodenv: application.nodenv,
				port: application.server.port
			});
		configureServerRoutes(server.router);
		queue.addCommand(server);
		queue.execute(callback);
	}
}

/**
 * @param {Router} router
 */
function configureServerRoutes(router) {
	router.get("/diagnostics/env", route_factory.create(route_diagnostics.EnvironmentRoute));
	router.get("/diagnostics/ping", route_factory.create(route_diagnostics.PingRoute));
	router.get("/metadata/exif/:rootPath([^$]+)", route_factory.create(route_content.GetExifMetadata));
	router.get("/metadata/ai/:rootPath([^$]+)", route_factory.create(route_content.GetAIMetadata));
}

/** local API **/
function startup() {
	const series=new SeriesQueue({verbose: true});
	series.addCommand(new SetupEnvironmentCommand());
	series.addCommand(new StartupCommand());
	series.execute(function(error) {
		if(error) {
			process.exit(1);
		}
	});
}

startup();
