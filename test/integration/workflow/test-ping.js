/**
* User: curt
* Date: 12/01/2017
* Time: 5:48 PM
*/

const assert=require("pig-core").assert;
const queue=require("pig-cmd").queue;
const request=require("pig-cmd").http.request;
const http=require("../../../src/common/http");
const {StartTestServerCommand, StopTestServerCommand}=require("../../support/command/spawn");


describe("workflow.ping", function() {
	it("should successfully spin up a server and ping it and shut it down", function(done) {
		const series=new queue.SeriesQueue(),
			pingUrl=http.route.diag.ping();
		series.addCommand(new StartTestServerCommand({}));
		series.addCommand(new request.HttpGetRequestCommand(pingUrl));
		series.addCommand(new StopTestServerCommand({}));
		series.execute(assert.isNotError(done));
	});
});


