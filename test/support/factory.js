/**
 * User: curtis
 * Date: 12/1/17
 * Time: 5:51 PM
 * Copyright @2017 by Xraymen Inc.
 */

const model=require("./model");
const constant=require("../../src/common/constant");
const model_factory=require("../../src/data/factory");


exports.application={
	/**
	 * @returns {Application}
	 */
	get: ()=>model_factory.application.get(constant.nodenv.TEST)
};

exports.httpRequest={
	/**
	 * Creates an instance of a mocked pig http request: model.http.Request
	 * @param {object} settings
	 * @returns {Request}
	 */
	create: function(settings={}) {
		return new model.http.Request(settings);
	}
};

exports.httpResponse={
	/**
	 * Creates an instance of mocked express response
	 * @param {Object} overrides
	 * @returns {ResponseCommand}
	 */
	create: function(overrides=undefined) {
		return new model.http.Response(overrides);
	}
};
