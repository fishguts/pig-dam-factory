/**
 * User: curtis
 * Date: 11/30/17
 * Time: 7:57 PM
 * Copyright @2017 by Xraymen Inc.
 */

const _=require("lodash");
const proxy=require("pig-core").proxy;

/**
 * An express IncomingMessage mock with optional pig params and body
 * @type {IncomingMessage}
 */
class HttpRequest {
	/**
	 * @param {Object} settings
	 * - headers {Object}
	 * - branch {String}
	 * - project {String}
	 * - user {String}
	 */
	constructor(settings) {
		Object.assign(this, _.pick(settings, ["headers"]));
		if(settings.branch) {
			_.set(this, "params.branch", settings.branch);
		}
		if(settings.project) {
			_.set(this, "params.project", settings.project);
		}
		if(settings.user) {
			_.set(this, "pig.user", settings.user);
		}
	}
	get branch() { return _.get(this.pig, "user"); }
	set branch(name) { _.set(this, "params.branch", name); }

	get project() { return _.get(this.pig, "user"); }
	set project(name) { _.set(this, "params.project", name); }

	get user() { return _.get(this.pig, "user"); }
	set user(name) { _.set(this, "pig.user", name); }
}

/**
 * An express ServerResponse mock
 * @type {ServerResponse}
 */
class HttpResponse {
	constructor() {
		this._statusCode=null;
		this._spy={
			json: proxy.spy(this, "json"),
			send: proxy.spy(this, "send"),
			sendFile: proxy.spy(this, "sendFile"),
			sendStatus: proxy.spy(this, "sendStatus")
		};
	}

	json(value) {}
	send(value) {}
	sendFile(value) {}
	sendStatus(value) {}

	get statusCode() { return this._statusCode; }
	set statusCode(v) { this._statusCode=v; }

	get spyJson() { return this._spy.json; }
	get spySend() { return this._spy.send; }
	get spySendFile() { return this._spy.sendFile; }
	get spySendStatus() { return this._spy.sendStatus; }

	getCallCount(fname) { return this._spy[fname].callCount; }
	getArguments(fname, call=0, index=-1) {
		return (index> -1)
			? this._spy[fname].getCall(call).args
			: this._spy[fname].getCall(call).args[index];
	}
}


exports.http={
	Request: HttpRequest,
	Response: HttpResponse
};
